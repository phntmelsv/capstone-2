const mongoose = require("mongoose")

const order_schema = new mongoose.Schema({
	userId: {
		type: String, // objectId
		required: [true, "User ID is required"]
	},
	products: [
		{
			productId: {
				type: String, // objectId
				required: [true, "Product ID is required"]
			},
			quantity: {
				type: Number,
				default: 1
			}
		}
	],

	// comment out - testing phase
	totalAmount: {
		type: Number,
		//required: [true, "Price is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("Order", order_schema)
