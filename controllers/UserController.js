const Order = require("../models/Order")
const User = require("../models/User")
const Product = require("../models/Product")
const auth = require('../auth')
const bcrypt = require("bcrypt")

module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then(result => {
		if(result.length > 0){
			return true 
		} 

		return false
	})
}

// user registration
module.exports.registerUser = (request_body) => {
	let new_user = new User({
		email: request_body.email,
		password: bcrypt.hashSync(request_body.password, 10) //hashSync function will 'hash' the password and turn it into random characters. The second argument serves as the amount of times that the password will be hashed. Note: The higher the number, the slower it is to 'unhash' which will affect application performance, and the lower the number the faster it will be.
	})

	return new_user.save().then((registered_user, error) => {
		if(error){
			return error 
		} 

		return 'User successfully registered!'
	})
}

// user auth
module.exports.loginUser = (request_body) => {
	return User.findOne({email: request_body.email}).then(result => {
		if(result === null){
			return "The user doesn't exist."
		}

		// We can't use regular comparison to check if password is correct because the password of the existing user is hashed with bcrypt. We have to use bcrypt to de-hash the password and compare it to the password from the request body. The compareSync() function will return either true or false depending on if they match.
		const is_password_correct = bcrypt.compareSync(request_body.password, result.password)

		if(is_password_correct){
			return {
				// Using the createAccessToken function, we can generate a token using the user data after its password has been validated.
				accessToken: auth.createAccessToken(result.toObject())
			}
		}

		return 'The email and password combination is not correct!'
	})
}

// For getting user details from the token
module.exports.getProfile = (userId) => {
	return User.findById(userId).then(result => {
		return result
	})
}

// For a user to create an order
module.exports.checkOut = async (request_body) => {
	// match user_id in User table to Order table
	let userSaveStatus = await User.findById(request_body.user_id)

	// match product_id in Product table to Order table
	let productSaveStatus = await Product.findById(request_body.product_id)


	// inserts the necessary product and user variables into the order table

	let new_order = new Order({
		userId: userSaveStatus.id,
		products: [
			{
				productId: productSaveStatus.id,
				quantity: request_body.quantity,
			}
		],

		totalAmount: productSaveStatus.price * request_body.quantity,
		purchasedOn: request_body.purchasedOn
	})

	return new_order.save().then((created_order, error) => {
			if(error) {
				return error
		}

		return {
			message: 'Order submitted successfully!',
			data: created_order
		}
	})



}


