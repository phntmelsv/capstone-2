const Product = require("../models/Product")

// Get ALL products
module.exports.getAll = () => {
	return Product.find({}).then(all_products => {
		return all_products
	})
}

// Get all ACTIVE products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(active_products => {
		return active_products
	})
}

// Create new product
module.exports.createProduct = (request_body) => {
	let new_product = new Product({
		name: request_body.name,
		description: request_body.description,
		price: request_body.price,
		isActive: request_body.isActive,
		createdOn: request_body.createdOn
	})

		return new_product.save().then((created_product, error) => {
			if(error) {
				return error
		}

		return {
			message: 'Product created successfully!',
			data: created_product
		}
	})
	
}

// Get single product
module.exports.getProduct = (product_id) => {
	return Product.findById(product_id).then(result => {
		return result
	})
}

// Update existing product
module.exports.updateProduct = (product_id, new_content) => {
	let updated_product = {
		name: new_content.name,
		description: new_content.description,
		price: new_content.price,
		isActive: new_content.isActive,
		createdOn: new_content.createdOn
	}

	return Product.findByIdAndUpdate(product_id, updated_product).then((modified_product, error) => {
		if(error){
			return error
		}

		return {
			message: "Product updated successfully!",
			data: modified_product
		}
	})
}

// Archiving a product
module.exports.archiveProduct = (product_id, new_content) => {
	let updated_product = {
		isActive: false
	}

	return Product.findByIdAndUpdate(product_id, updated_product).then((modified_product, error) => {
		if(error){
			return error
		}

		return {
			message: "Product updated successfully!",
			data: modified_product
		}
	})
}

// Activating a product
module.exports.activateProduct = (product_id, new_content) => {
	let updated_product = {
		isActive: true
	}

	return Product.findByIdAndUpdate(product_id, updated_product).then((modified_product, error) => {
		if(error){
			return error
		}

		return {
			message: "Product updated successfully!",
			data: modified_product
		}
	})
}