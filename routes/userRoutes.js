const express = require("express")
const router = express.Router()
const auth = require("../auth")
const UserController = require("../controllers/UserController")

// Check email for existing user
router.post("/check-email", (request, response) => {
	UserController.checkEmailExists(request.body).then(result => response.send(result))
})

// Register user
router.post("/register", (request, response) => {
	UserController.registerUser(request.body).then(result => response.send(result))
})

// Login user
router.post("/login", (request, response) => {
	UserController.loginUser(request.body).then(result => response.send(result))
})

// Get user details from token
router.get("/details", auth.verify, (request, response) => {
	// decode function will return the user data from the token and assign it to the 'user_data' variable
	const user_data = auth.decode(request.headers.authorization)

	// We pass the id from the user_data variable to get all of the details of that user from the database
	UserController.getProfile(user_data.id).then(result => response.send(result))
})

// Non-admin user checkout (create order)

	// get user id from user,
	// get price and product_id from products
	// place in orders data model
router.post("/checkout", auth.verify, (request, response) => {
	let request_body = {
		user_id: auth.decode(request.headers.authorization).id,
		product_id: request.body.productId,

		// comment out - testing phase
		quantity: request.body.quantity,
		totalAmount: request.body.quantity * request.body.price,
		purchasedOn: request.body.purchasedOn
	}

	UserController.checkOut(request_body).then(result => response.send(result))
})

module.exports = router