const express = require("express")
const router = express.Router()
const productController = require("../controllers/ProductController")
const auth = require("../auth")

// Get all products
router.get("/all", (request, response) => {
	productController.getAll().then(result => response.send(result))
})

// Get all ACTIVE products
router.get("/", (request,response) => {
	productController.getAllActive().then(result => response.send(result))
})

// Create new product
router.post("/", auth.verify, (request,response) => {
	productController.createProduct(request.body).then(result => response.send(result))

})

// Get single product
router.get("/:id", (request, response) => {
	productController.getProduct(request.params.id).then(result => response.send(result))
})

// Update details of existing product
router.patch("/:id/update", auth.verify, (request, response) => {
	productController.updateProduct(request.params.id,request.body).then(result => response.send(result))
})


// Archiving a product
router.patch("/:id/archive", auth.verify, (request, response) => {
	productController.archiveProduct(request.params.id,request.body).then(result => response.send(result))
})

// Activate product (admin only)
router.patch("/:id/activate", auth.verify, (request, response) => {
	productController.activateProduct(request.params.id,request.body).then(result => response.send(result))
})


module.exports = router